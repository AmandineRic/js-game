/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

const fetchData = (amount, category, difficulty) => {
  const baseURL = "https://opentdb.com/api.php";
  let URL;
  if (category === "any" && difficulty === "any") {
    URL = `${baseURL}?amount=${amount}&type=multiple`;
  } else if (difficulty === "any") {
    URL = `${baseURL}?amount=${amount}&category=${category}&type=multiple`;
  } else if (category === "any") {
    URL = `${baseURL}?amount=${amount}&difficulty=${difficulty}&type=multiple`;
  } else {
    URL = `${baseURL}?amount=${amount}&category=${category}&difficulty=${difficulty}&type=multiple`;
  }

  fetch(URL)
    .then(response => {
      return response.json();
    })
    .then(payload => {
      quizQuestions = JSON.parse(JSON.stringify(payload));
      document.getElementById("url-generator").classList.toggle("d-none");
      document.getElementById("start").classList.toggle("d-none");
    })
    .catch(error => console.log(error));
};

document.addEventListener("DOMContentLoaded", function () {
  document.getElementById("url-generator").addEventListener("click", event => {
    let difficulty = document.getElementById("difficulty").value;
    let amount = document.getElementById("amount").value;
    let category = document.getElementById("category").value;
    fetchData(amount, category, difficulty);
  });
});

// Initial values
let counter = 30;
let currentQuestion = 0;
let score = 0;
let lost = 0;
let timer;

// If the timer is over, then go to the next question
function nextQuestion() {
  const isQuestionOver = quizQuestions.results.length - 1 === currentQuestion;
  if (isQuestionOver) {
    displayResult();
  } else {
    currentQuestion++;
    loadQuestion();
  }
}

// Start a 30 seconds timer for user to respond or choose an answer to each question
function timeUp() {
  clearInterval(timer);

  lost++;

  displayAnswer("lost");
  setTimeout(nextQuestion, 3 * 1000);
}

function countDown() {
  counter--;
  document.getElementById("time").innerHTML = `Timer:  ${counter}`;

  if (counter === 0) {
    timeUp();
  }
}

// Display the question and the choices to the browser
function loadQuestion() {
  counter = 30;
  timer = setInterval(countDown, 1000);

  let question = quizQuestions.results[currentQuestion].question;
  const choices = [
    ...quizQuestions.results[currentQuestion].incorrect_answers,
    quizQuestions.results[currentQuestion].correct_answer,

  ];

  shuffle(choices);

  document.getElementById("time").innerHTML = `Timer:  ${counter}`;
  document.getElementById("game").innerHTML = `
  <h4>${question}</h4>
  ${loadChoices(choices)}
  ${loadQuestionCount()}`;

  choiceSelect();
}


function loadChoices(choices) {
  let result = "";

  for (let i = 0; i < choices.length; i++) {
    result += `<p class="choice" data-answer="${choices[i]}">${choices[i]}</p>`;
  }

  return result;
}

// Either correct/wrong choice selected, go to the next question
const choiceSelect = function () {
  const choiceButtons = Array.from(document.getElementsByClassName("choice"));
  
  choiceButtons.forEach(element => {
    element.addEventListener("click", function () {
      clearInterval(timer);
      const selectedAnswer = this.getAttribute("data-answer");
      const correctAnswer = quizQuestions.results[currentQuestion].correct_answer;

      if (correctAnswer === selectedAnswer) {
        score++;
        displayAnswer("win");
        setTimeout(nextQuestion, 3 * 1000);
      } else {
        lost++;
        displayAnswer("lost");
        setTimeout(nextQuestion, 3 * 1000);
      }
    })
  })
}



function displayResult() {
  const result = `
        <p>You've got ${score} question(s) right on ${quizQuestions.results.length} questions</p>
    `;

  document.getElementById("game").innerHTML = ``;
  document.getElementById("time").innerHTML = ``;
  document.getElementById("reset").classList.toggle("d-none");
  document.getElementById("displayGameOver").classList.toggle("d-none");
  document.getElementById("newGame").classList.toggle("d-none");
  document.getElementById("displayGameOver").innerHTML = result;
}

document.getElementById("newGame").addEventListener("click", event => {
  counter = 30;
  currentQuestion = 0;
  score = 0;
  lost = 0;
  timer = null;

  document.getElementById("reset").classList.toggle("d-none");
  document.getElementById("newGame").classList.toggle("d-none");
  document.getElementById("url-generator").classList.toggle("d-none");
  document.getElementById("form").classList.toggle("d-none");
  document.getElementById("displayGameOver").classList.toggle("d-none");
  document.getElementById("displayGameOver").innerHTML = "";
});

document.getElementById("reset").addEventListener("click", event => {
  counter = 30;
  currentQuestion = 0;
  score = 0;
  lost = 0;
  timer = null;

  document.getElementById("reset").classList.toggle("d-none");
  document.getElementById("newGame").classList.toggle("d-none");
  document.getElementById("displayGameOver").classList.toggle("d-none");
  document.getElementById("displayGameOver").innerHTML = "";

  loadQuestion();
});

// Display question counter

function loadQuestionCount() {
  const totalQuestion = quizQuestions.results.length;

  return `Question ${currentQuestion + 1} of ${totalQuestion}`;
}

// Display correct and wrong answers

function displayAnswer(status) {
  const correctAnswer = quizQuestions.results[currentQuestion].correct_answer;

  if (status === "win") {
    document.getElementById(
      "game"
    ).innerHTML = `<p>Congratulations, you pick the corrrect answer</p><p>The correct answer is <span>${correctAnswer}</span></p>`;
  } else {
    document.getElementById(
      "game"
    ).innerHTML = `<p>The correct answer was <span>${correctAnswer}</span></p><p>You lost pretty bad</p>`;
  }
}

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

document.getElementById("start").addEventListener("click", event => {
  document.getElementById("start").classList.toggle("d-none");
  document.getElementById("form").classList.toggle("d-none");
  document.getElementById("time").innerHTML = counter;

  loadQuestion();
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUSxVQUFVLE9BQU87QUFDdEMsR0FBRztBQUNILGFBQWEsUUFBUSxVQUFVLE9BQU8sWUFBWSxTQUFTO0FBQzNELEdBQUc7QUFDSCxhQUFhLFFBQVEsVUFBVSxPQUFPLGNBQWMsV0FBVztBQUMvRCxHQUFHO0FBQ0gsYUFBYSxRQUFRLFVBQVUsT0FBTyxZQUFZLFNBQVMsY0FBYyxXQUFXO0FBQ3BGOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHlEQUF5RCxRQUFROztBQUVqRTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSx5REFBeUQsUUFBUTtBQUNqRTtBQUNBLFFBQVEsU0FBUztBQUNqQixJQUFJO0FBQ0osSUFBSSxvQkFBb0I7O0FBRXhCO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUEsaUJBQWlCLG9CQUFvQjtBQUNyQyxnREFBZ0QsV0FBVyxJQUFJLFdBQVc7QUFDMUU7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7Ozs7QUFJQTtBQUNBO0FBQ0Esd0JBQXdCLE1BQU0sd0JBQXdCLDZCQUE2QjtBQUNuRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7O0FBRUEscUJBQXFCLG9CQUFvQixNQUFNLGNBQWM7QUFDN0Q7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx3R0FBd0csY0FBYztBQUN0SCxHQUFHO0FBQ0g7QUFDQTtBQUNBLHFEQUFxRCxjQUFjO0FBQ25FO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDQUFDLEUiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiY29uc3QgZmV0Y2hEYXRhID0gKGFtb3VudCwgY2F0ZWdvcnksIGRpZmZpY3VsdHkpID0+IHtcbiAgY29uc3QgYmFzZVVSTCA9IFwiaHR0cHM6Ly9vcGVudGRiLmNvbS9hcGkucGhwXCI7XG4gIGxldCBVUkw7XG4gIGlmIChjYXRlZ29yeSA9PT0gXCJhbnlcIiAmJiBkaWZmaWN1bHR5ID09PSBcImFueVwiKSB7XG4gICAgVVJMID0gYCR7YmFzZVVSTH0/YW1vdW50PSR7YW1vdW50fSZ0eXBlPW11bHRpcGxlYDtcbiAgfSBlbHNlIGlmIChkaWZmaWN1bHR5ID09PSBcImFueVwiKSB7XG4gICAgVVJMID0gYCR7YmFzZVVSTH0/YW1vdW50PSR7YW1vdW50fSZjYXRlZ29yeT0ke2NhdGVnb3J5fSZ0eXBlPW11bHRpcGxlYDtcbiAgfSBlbHNlIGlmIChjYXRlZ29yeSA9PT0gXCJhbnlcIikge1xuICAgIFVSTCA9IGAke2Jhc2VVUkx9P2Ftb3VudD0ke2Ftb3VudH0mZGlmZmljdWx0eT0ke2RpZmZpY3VsdHl9JnR5cGU9bXVsdGlwbGVgO1xuICB9IGVsc2Uge1xuICAgIFVSTCA9IGAke2Jhc2VVUkx9P2Ftb3VudD0ke2Ftb3VudH0mY2F0ZWdvcnk9JHtjYXRlZ29yeX0mZGlmZmljdWx0eT0ke2RpZmZpY3VsdHl9JnR5cGU9bXVsdGlwbGVgO1xuICB9XG5cbiAgZmV0Y2goVVJMKVxuICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7XG4gICAgfSlcbiAgICAudGhlbihwYXlsb2FkID0+IHtcbiAgICAgIHF1aXpRdWVzdGlvbnMgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHBheWxvYWQpKTtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidXJsLWdlbmVyYXRvclwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzdGFydFwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICAgIH0pXG4gICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSk7XG59O1xuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCBmdW5jdGlvbiAoKSB7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidXJsLWdlbmVyYXRvclwiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZXZlbnQgPT4ge1xuICAgIGxldCBkaWZmaWN1bHR5ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkaWZmaWN1bHR5XCIpLnZhbHVlO1xuICAgIGxldCBhbW91bnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFtb3VudFwiKS52YWx1ZTtcbiAgICBsZXQgY2F0ZWdvcnkgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNhdGVnb3J5XCIpLnZhbHVlO1xuICAgIGZldGNoRGF0YShhbW91bnQsIGNhdGVnb3J5LCBkaWZmaWN1bHR5KTtcbiAgfSk7XG59KTtcblxuLy8gSW5pdGlhbCB2YWx1ZXNcbmxldCBjb3VudGVyID0gMzA7XG5sZXQgY3VycmVudFF1ZXN0aW9uID0gMDtcbmxldCBzY29yZSA9IDA7XG5sZXQgbG9zdCA9IDA7XG5sZXQgdGltZXI7XG5cbi8vIElmIHRoZSB0aW1lciBpcyBvdmVyLCB0aGVuIGdvIHRvIHRoZSBuZXh0IHF1ZXN0aW9uXG5mdW5jdGlvbiBuZXh0UXVlc3Rpb24oKSB7XG4gIGNvbnN0IGlzUXVlc3Rpb25PdmVyID0gcXVpelF1ZXN0aW9ucy5yZXN1bHRzLmxlbmd0aCAtIDEgPT09IGN1cnJlbnRRdWVzdGlvbjtcbiAgaWYgKGlzUXVlc3Rpb25PdmVyKSB7XG4gICAgZGlzcGxheVJlc3VsdCgpO1xuICB9IGVsc2Uge1xuICAgIGN1cnJlbnRRdWVzdGlvbisrO1xuICAgIGxvYWRRdWVzdGlvbigpO1xuICB9XG59XG5cbi8vIFN0YXJ0IGEgMzAgc2Vjb25kcyB0aW1lciBmb3IgdXNlciB0byByZXNwb25kIG9yIGNob29zZSBhbiBhbnN3ZXIgdG8gZWFjaCBxdWVzdGlvblxuZnVuY3Rpb24gdGltZVVwKCkge1xuICBjbGVhckludGVydmFsKHRpbWVyKTtcblxuICBsb3N0Kys7XG5cbiAgZGlzcGxheUFuc3dlcihcImxvc3RcIik7XG4gIHNldFRpbWVvdXQobmV4dFF1ZXN0aW9uLCAzICogMTAwMCk7XG59XG5cbmZ1bmN0aW9uIGNvdW50RG93bigpIHtcbiAgY291bnRlci0tO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRpbWVcIikuaW5uZXJIVE1MID0gYFRpbWVyOiAgJHtjb3VudGVyfWA7XG5cbiAgaWYgKGNvdW50ZXIgPT09IDApIHtcbiAgICB0aW1lVXAoKTtcbiAgfVxufVxuXG4vLyBEaXNwbGF5IHRoZSBxdWVzdGlvbiBhbmQgdGhlIGNob2ljZXMgdG8gdGhlIGJyb3dzZXJcbmZ1bmN0aW9uIGxvYWRRdWVzdGlvbigpIHtcbiAgY291bnRlciA9IDMwO1xuICB0aW1lciA9IHNldEludGVydmFsKGNvdW50RG93biwgMTAwMCk7XG5cbiAgbGV0IHF1ZXN0aW9uID0gcXVpelF1ZXN0aW9ucy5yZXN1bHRzW2N1cnJlbnRRdWVzdGlvbl0ucXVlc3Rpb247XG4gIGNvbnN0IGNob2ljZXMgPSBbXG4gICAgLi4ucXVpelF1ZXN0aW9ucy5yZXN1bHRzW2N1cnJlbnRRdWVzdGlvbl0uaW5jb3JyZWN0X2Fuc3dlcnMsXG4gICAgcXVpelF1ZXN0aW9ucy5yZXN1bHRzW2N1cnJlbnRRdWVzdGlvbl0uY29ycmVjdF9hbnN3ZXIsXG5cbiAgXTtcblxuICBzaHVmZmxlKGNob2ljZXMpO1xuXG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidGltZVwiKS5pbm5lckhUTUwgPSBgVGltZXI6ICAke2NvdW50ZXJ9YDtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJnYW1lXCIpLmlubmVySFRNTCA9IGBcbiAgPGg0PiR7cXVlc3Rpb259PC9oND5cbiAgJHtsb2FkQ2hvaWNlcyhjaG9pY2VzKX1cbiAgJHtsb2FkUXVlc3Rpb25Db3VudCgpfWA7XG5cbiAgY2hvaWNlU2VsZWN0KCk7XG59XG5cblxuZnVuY3Rpb24gbG9hZENob2ljZXMoY2hvaWNlcykge1xuICBsZXQgcmVzdWx0ID0gXCJcIjtcblxuICBmb3IgKGxldCBpID0gMDsgaSA8IGNob2ljZXMubGVuZ3RoOyBpKyspIHtcbiAgICByZXN1bHQgKz0gYDxwIGNsYXNzPVwiY2hvaWNlXCIgZGF0YS1hbnN3ZXI9XCIke2Nob2ljZXNbaV19XCI+JHtjaG9pY2VzW2ldfTwvcD5gO1xuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuLy8gRWl0aGVyIGNvcnJlY3Qvd3JvbmcgY2hvaWNlIHNlbGVjdGVkLCBnbyB0byB0aGUgbmV4dCBxdWVzdGlvblxuY29uc3QgY2hvaWNlU2VsZWN0ID0gZnVuY3Rpb24gKCkge1xuICBjb25zdCBjaG9pY2VCdXR0b25zID0gQXJyYXkuZnJvbShkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwiY2hvaWNlXCIpKTtcbiAgXG4gIGNob2ljZUJ1dHRvbnMuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBjbGVhckludGVydmFsKHRpbWVyKTtcbiAgICAgIGNvbnN0IHNlbGVjdGVkQW5zd2VyID0gdGhpcy5nZXRBdHRyaWJ1dGUoXCJkYXRhLWFuc3dlclwiKTtcbiAgICAgIGNvbnN0IGNvcnJlY3RBbnN3ZXIgPSBxdWl6UXVlc3Rpb25zLnJlc3VsdHNbY3VycmVudFF1ZXN0aW9uXS5jb3JyZWN0X2Fuc3dlcjtcblxuICAgICAgaWYgKGNvcnJlY3RBbnN3ZXIgPT09IHNlbGVjdGVkQW5zd2VyKSB7XG4gICAgICAgIHNjb3JlKys7XG4gICAgICAgIGRpc3BsYXlBbnN3ZXIoXCJ3aW5cIik7XG4gICAgICAgIHNldFRpbWVvdXQobmV4dFF1ZXN0aW9uLCAzICogMTAwMCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBsb3N0Kys7XG4gICAgICAgIGRpc3BsYXlBbnN3ZXIoXCJsb3N0XCIpO1xuICAgICAgICBzZXRUaW1lb3V0KG5leHRRdWVzdGlvbiwgMyAqIDEwMDApO1xuICAgICAgfVxuICAgIH0pXG4gIH0pXG59XG5cblxuXG5mdW5jdGlvbiBkaXNwbGF5UmVzdWx0KCkge1xuICBjb25zdCByZXN1bHQgPSBgXG4gICAgICAgIDxwPllvdSd2ZSBnb3QgJHtzY29yZX0gcXVlc3Rpb24ocykgcmlnaHQgb24gJHtxdWl6UXVlc3Rpb25zLnJlc3VsdHMubGVuZ3RofSBxdWVzdGlvbnM8L3A+XG4gICAgYDtcblxuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImdhbWVcIikuaW5uZXJIVE1MID0gYGA7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidGltZVwiKS5pbm5lckhUTUwgPSBgYDtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJyZXNldFwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpc3BsYXlHYW1lT3ZlclwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm5ld0dhbWVcIikuY2xhc3NMaXN0LnRvZ2dsZShcImQtbm9uZVwiKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkaXNwbGF5R2FtZU92ZXJcIikuaW5uZXJIVE1MID0gcmVzdWx0O1xufVxuXG5kb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm5ld0dhbWVcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGV2ZW50ID0+IHtcbiAgY291bnRlciA9IDMwO1xuICBjdXJyZW50UXVlc3Rpb24gPSAwO1xuICBzY29yZSA9IDA7XG4gIGxvc3QgPSAwO1xuICB0aW1lciA9IG51bGw7XG5cbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJyZXNldFwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm5ld0dhbWVcIikuY2xhc3NMaXN0LnRvZ2dsZShcImQtbm9uZVwiKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ1cmwtZ2VuZXJhdG9yXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJkLW5vbmVcIik7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZm9ybVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpc3BsYXlHYW1lT3ZlclwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpc3BsYXlHYW1lT3ZlclwiKS5pbm5lckhUTUwgPSBcIlwiO1xufSk7XG5cbmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicmVzZXRcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGV2ZW50ID0+IHtcbiAgY291bnRlciA9IDMwO1xuICBjdXJyZW50UXVlc3Rpb24gPSAwO1xuICBzY29yZSA9IDA7XG4gIGxvc3QgPSAwO1xuICB0aW1lciA9IG51bGw7XG5cbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJyZXNldFwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm5ld0dhbWVcIikuY2xhc3NMaXN0LnRvZ2dsZShcImQtbm9uZVwiKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkaXNwbGF5R2FtZU92ZXJcIikuY2xhc3NMaXN0LnRvZ2dsZShcImQtbm9uZVwiKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkaXNwbGF5R2FtZU92ZXJcIikuaW5uZXJIVE1MID0gXCJcIjtcblxuICBsb2FkUXVlc3Rpb24oKTtcbn0pO1xuXG4vLyBEaXNwbGF5IHF1ZXN0aW9uIGNvdW50ZXJcblxuZnVuY3Rpb24gbG9hZFF1ZXN0aW9uQ291bnQoKSB7XG4gIGNvbnN0IHRvdGFsUXVlc3Rpb24gPSBxdWl6UXVlc3Rpb25zLnJlc3VsdHMubGVuZ3RoO1xuXG4gIHJldHVybiBgUXVlc3Rpb24gJHtjdXJyZW50UXVlc3Rpb24gKyAxfSBvZiAke3RvdGFsUXVlc3Rpb259YDtcbn1cblxuLy8gRGlzcGxheSBjb3JyZWN0IGFuZCB3cm9uZyBhbnN3ZXJzXG5cbmZ1bmN0aW9uIGRpc3BsYXlBbnN3ZXIoc3RhdHVzKSB7XG4gIGNvbnN0IGNvcnJlY3RBbnN3ZXIgPSBxdWl6UXVlc3Rpb25zLnJlc3VsdHNbY3VycmVudFF1ZXN0aW9uXS5jb3JyZWN0X2Fuc3dlcjtcblxuICBpZiAoc3RhdHVzID09PSBcIndpblwiKSB7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXG4gICAgICBcImdhbWVcIlxuICAgICkuaW5uZXJIVE1MID0gYDxwPkNvbmdyYXR1bGF0aW9ucywgeW91IHBpY2sgdGhlIGNvcnJyZWN0IGFuc3dlcjwvcD48cD5UaGUgY29ycmVjdCBhbnN3ZXIgaXMgPHNwYW4+JHtjb3JyZWN0QW5zd2VyfTwvc3Bhbj48L3A+YDtcbiAgfSBlbHNlIHtcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcbiAgICAgIFwiZ2FtZVwiXG4gICAgKS5pbm5lckhUTUwgPSBgPHA+VGhlIGNvcnJlY3QgYW5zd2VyIHdhcyA8c3Bhbj4ke2NvcnJlY3RBbnN3ZXJ9PC9zcGFuPjwvcD48cD5Zb3UgbG9zdCBwcmV0dHkgYmFkPC9wPmA7XG4gIH1cbn1cblxuZnVuY3Rpb24gc2h1ZmZsZShhcnJheSkge1xuICB2YXIgY3VycmVudEluZGV4ID0gYXJyYXkubGVuZ3RoLFxuICAgIHRlbXBvcmFyeVZhbHVlLFxuICAgIHJhbmRvbUluZGV4O1xuXG4gIC8vIFdoaWxlIHRoZXJlIHJlbWFpbiBlbGVtZW50cyB0byBzaHVmZmxlLi4uXG4gIHdoaWxlICgwICE9PSBjdXJyZW50SW5kZXgpIHtcbiAgICAvLyBQaWNrIGEgcmVtYWluaW5nIGVsZW1lbnQuLi5cbiAgICByYW5kb21JbmRleCA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIGN1cnJlbnRJbmRleCk7XG4gICAgY3VycmVudEluZGV4IC09IDE7XG5cbiAgICAvLyBBbmQgc3dhcCBpdCB3aXRoIHRoZSBjdXJyZW50IGVsZW1lbnRcbiAgICB0ZW1wb3JhcnlWYWx1ZSA9IGFycmF5W2N1cnJlbnRJbmRleF07XG4gICAgYXJyYXlbY3VycmVudEluZGV4XSA9IGFycmF5W3JhbmRvbUluZGV4XTtcbiAgICBhcnJheVtyYW5kb21JbmRleF0gPSB0ZW1wb3JhcnlWYWx1ZTtcbiAgfVxuXG4gIHJldHVybiBhcnJheTtcbn1cblxuZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzdGFydFwiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZXZlbnQgPT4ge1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInN0YXJ0XCIpLmNsYXNzTGlzdC50b2dnbGUoXCJkLW5vbmVcIik7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZm9ybVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwiZC1ub25lXCIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRpbWVcIikuaW5uZXJIVE1MID0gY291bnRlcjtcblxuICBsb2FkUXVlc3Rpb24oKTtcbn0pOyJdLCJzb3VyY2VSb290IjoiIn0=